import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.lang.builder import Builder

Builder.load_file("./my.kv")

class Calculator(Widget):
    def clear(self):
        self.ids.text_input.text = '0'
    def button_value(self,number):
        prev_number = self.ids.text_input.text
        if "invalid eqaution" in prev_number:
            prev_number=''
        if prev_number == "0":
            self.ids.text_input.text = ""
            self.ids.text_input.text =f"{number}"
        else:
            self.ids.text_input.text =f"{prev_number}"f"{number}"
    def operation(self,operation):
        prev_number = self.ids.text_input.text
        self.ids.text_input.text = f"{prev_number}{operation}"
    def answer(self):
        prev_number = self.ids.text_input.text
        try:
            result = eval(prev_number)
            self.ids.text_input.text = str(result)
        except:
            self.ids.text_input.text = "invalid eqaution"
class myapp(App):
    def build(self):
        return Calculator()

if __name__ == "__main__":
    myapp().run()

